import React from "react";
import { ConfigProvider } from "antd";

const MixedTheme = (node: JSX.Element) => (
  <>
  
  <ConfigProvider
        theme={{
          token: {                
            colorTextDescription:'#ef8200',
            colorText: '#2e2e2e',
            colorIcon: "#fff"
          },
        }}
      >
        <ConfigProvider
          theme={{
            components:{
              Drawer:{
                  colorBgElevated:'#F5d994',
                  colorBgBase:'#d7705f',
              },
              Button:{
                colorText:'#2e2e2e'
              
        
                  
              },
              Card: {
                colorBorderSecondary:'#fff'
              },
              Layout:{
                colorBgBody:'#fff',
                colorText:'#2e2e2e',
                colorTextDescription:'#2e2e2e'
              },
              FloatButton:{
                colorBgElevated:'black',
              },
              Collapse:{
                colorBgContainer: '#F7F7F7'
              }
            }
          }}
        >
          {node}
        </ConfigProvider>
      </ConfigProvider>

      <ConfigProvider  
      theme={{
          token: {                
            colorText: '#fff',
            colorIcon: "#fff",
            colorBgContainer: '#2e2e2e'
          },
        }}
        >
     
        <ConfigProvider
        theme={{
            components: {
              Drawer:{
                colorBgElevated:'#2e2e2e',
                colorText: '#d7705f',
                colorIcon:'#d7705f'
              },
              Card:{
                colorBorderSecondary:'#2e2e2e'
              },
              Layout:{
                colorBgBody: '#2e2e2e',
                colorText: '#fff'
              },
              FloatButton:{
                colorBgElevated:'white',
            },
              Modal: {
          
                contentBg: '#2e2e2e',  
                headerBg: '#2e2e2e',
                
          
              },
              Input:{
                colorBgContainer : '#454545',
                colorTextPlaceholder: '#7F7F7F',
                
                
                
              },
              Button:{
                colorBgContainer : '#424242',
                boxShadow: '0px',
                colorText: 'white',
                colorBgLayout: 'white',
                colorBgBase: 'white',
                colorFill: 'white',
                colorFillAlter: 'white',
                colorPrimaryBg: 'white',
                colorBgContainerDisabled: 'white',
                colorInfo: 'white',
                colorTextBase: 'white',
                colorBgElevated: 'white',
                colorIcon: 'white',
                colorPrimary: 'white'
          
        
              },
              
            }
          }}
          >
          {node}
        </ConfigProvider>
      </ConfigProvider> 
  </>
  )

export default MixedTheme;