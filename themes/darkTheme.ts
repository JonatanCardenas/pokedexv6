import { ThemeConfig, Card, Button } from 'antd';
import { ListItemIcon } from '@mui/material';
import { FilterOutlined, SearchOutlined } from '@ant-design/icons';

export const darkTheme: ThemeConfig = {
  token: {
    colorText: '#fff',
    colorIcon: "#fff",
    colorBgContainer: '#2e2e2e'
  },
  components: {
    Drawer:{
      colorBgElevated:'#2e2e2e',
      colorText: '#d7705f',
      colorIcon:'#d7705f'
    },
    Card:{
      colorBorderSecondary:'#2e2e2e'
    },
    Layout:{
      colorBgBody: '#2e2e2e',
      colorText: '#fff'
    },
    FloatButton:{
      colorBgElevated:'white',
   },
    Modal: {

      contentBg: '#2e2e2e',  
      headerBg: '#2e2e2e',
      

    },
    Input:{
      colorBgContainer : '#454545',
      colorTextPlaceholder: '#7F7F7F',
      
      
      
    },
    Button:{
      colorBgContainer : '#424242',
      boxShadow: '0px',
      colorText: 'white',
      colorBgLayout: 'white',
      colorBgBase: 'white',
      colorFill: 'white',
      colorFillAlter: 'white',
      colorPrimaryBg: 'white',
      colorBgContainerDisabled: 'white',
      colorInfo: 'white',
      colorTextBase: 'white',
      colorBgElevated: 'white',
      colorIcon: 'white',
      colorPrimary: 'white'


      
     
    },
    
    
   }
  
}
