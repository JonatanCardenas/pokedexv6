export interface PokemonGQLResponse {
    data: Data;
}

export interface Data {
    pokemon_v2_pokemon: PokemonV2Pokemon[];
}

export interface PokemonV2Pokemon {
    id: number;
    name: string;
    height: number;
    weight: number;
    pokemon_v2_encounters: PokemonV2Encounter[];
    pokemon_v2_pokemonstats: PokemonV2Pokemonstat[];
    pokemon_v2_pokemontypes: PokemonV2Pokemontype[];
}

export interface PokemonV2Encounter {
    pokemon_v2_locationarea: PokemonV2Locationarea;
    location_area_id: number;
}

export interface PokemonV2Locationarea {
    pokemon_v2_location: PokemonV2Location;
}

export interface PokemonV2Location {
    pokemon_v2_region: PokemonV2Region;
}

export interface PokemonV2Region {
    name: PokemonV2RegionName;
    id: number;
}

export enum PokemonV2RegionName {
    Hoenn = "hoenn",
    Johto = "johto",
    Kalos = "kalos",
    Kanto = "kanto",
    Sinnoh = "sinnoh",
    Unova = "unova",
}

export interface PokemonV2Pokemonstat {
    pokemon_v2_stat: PokemonV2;
    base_stat: number;
}

export interface PokemonV2 {
    name: PokemonV2StatName;
}

export enum PokemonV2StatName {
    Attack = "attack",
    Bug = "bug",
    Defense = "defense",
    Dragon = "dragon",
    Electric = "electric",
    Fairy = "fairy",
    Fighting = "fighting",
    Fire = "fire",
    Flying = "flying",
    Ghost = "ghost",
    Grass = "grass",
    Ground = "ground",
    HP = "hp",
    Ice = "ice",
    Normal = "normal",
    Poison = "poison",
    Psychic = "psychic",
    Rock = "rock",
    SpecialAttack = "special-attack",
    SpecialDefense = "special-defense",
    Speed = "speed",
    Steel = "steel",
    Water = "water",
}

export interface PokemonV2Pokemontype {
    pokemon_v2_type: PokemonV2;
}
