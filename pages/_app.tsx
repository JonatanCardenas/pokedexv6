import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { useEffect, useState, useContext  } from "react";
import { ConfigProvider, FloatButton, ThemeConfig, theme } from 'antd';
import { darkTheme, lightTheme  } from '@/themes';
import { BackwardOutlined, ForwardOutlined } from '@ant-design/icons';
import localTheme from '@/utils/localTheme';
import Loading from './Loading';
import { EntriesProvider } from '@/context/entries';
import { StyleProvider } from '@ant-design/cssinjs';
import '../public/antd.min.css';
import '../styles/globals.css'; // add this line
import { EntriesContext } from "@/context/entries";


function App({ Component, pageProps}: AppProps) {
  
  const [isDarkMode, setIsDarkMode] = useState(false);

  useEffect(() => {
    //import('../public/js/push')
    const themeCurrent = localTheme.getLocalTheme();
    if (!themeCurrent) return setIsDarkMode(false);
    setIsDarkMode(themeCurrent.darkTheme);
  }, []);

  const handleClick = () => {
    localTheme.setLocalTheme(!isDarkMode);
    setIsDarkMode(!isDarkMode);
  };
  
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {

    setTimeout(() => {
      setIsLoading(false);
    }, 2500);
  }, [])
  return (
    <div style={{ color: 'red' }}>
      {isLoading === true ?
        <Loading /> :
        <EntriesProvider>
          <ConfigProvider theme={isDarkMode ? darkTheme : lightTheme}>
            <FloatButton
              shape="circle"
              style={{ right: 20 }}
              onClick={handleClick}
              icon={isDarkMode ? <BackwardOutlined style={{ color: 'black' }} /> : <ForwardOutlined style={{ color: 'white' }} />} />
            <Component  {...pageProps}>
            </Component>
          </ConfigProvider>
        </EntriesProvider>

      }
    </div>
  )

}

export default App

