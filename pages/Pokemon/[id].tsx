import { DetalleLayout } from "@/components/layouts";
import { PokemonList } from "@/interfaces";
import localPokemon from "@/utils/localPokemon";
import {
    Col,
    Divider,
    Row,
    Switch,
    Typography
} from "antd";
import { useContext, useEffect, useState } from 'react';
import Loading from "../Loading";
import pokeApi from "@/utils/pokeApi";
import { EntriesContext } from "@/context/entries";
import {
    ContenidoPokemon,
    DetallePrincipalPokemon,
    NoEncontrado,
} from "@/components/pokemon";


const initial_vacia: PokemonList = {
    name: '',
    catch: false,
    height: 12,
    id: 1,
    img: '',
    regions: [],
    stats: [],
    types: [],
    weight: 12
}

const PokemonDetalle = () => {
    const { cantidadPoke } = useContext(EntriesContext)
    const [capture, setCapture] = useState(false);
    const [pokemon, setPokemon] = useState<PokemonList>(initial_vacia);
    const { isLoading, isLoadingChange } = useContext(EntriesContext);
    useEffect(() => {
        import('../../public/js/push.js');
        let id: string = "";
        var input_string = window.location.href;
        id = input_string.substring(input_string.lastIndexOf('/') + 1);
        const getCachePokemon = async (id: number) => {
            let pokemones: PokemonList[] = [];
            await isLoadingChange(true);
            if (!navigator.onLine) {
                pokemones = await pokeApi.getPokemonApi(cantidadPoke);
            } else {
                pokemones = await pokeApi.getPokemonByIdApi(id);
            }
            await setPokemon(pokemones.find(x => x.id === id)!)
            const data = await localPokemon.catchNow(id);
            await setCapture(data)
            await isLoadingChange(false);
        }
        getCachePokemon(parseInt(id));

    }, [])
    if (pokemon == undefined) return <NoEncontrado />
    return (
        (isLoading ? <Loading /> : (

            <DetalleLayout title={pokemon.name}>
                <DetallePrincipalPokemon
                    id={pokemon.id}
                    img={pokemon.img}
                    name={pokemon.name}
                    types={pokemon.types}
                />
                <ContenidoPokemon pokemon={pokemon} />
                <Divider style={{ background: 'white' }} />
                <Row style={{ padding: '10px' }} justify={'end'} >
                    <Col span={24} style={{ textAlign: 'center' }} >
                        <Typography.Title level={4} >CATCH</Typography.Title>
                        <Switch checked={capture} onChange={() => (localPokemon.setLocalPokemon(capture, pokemon, cantidadPoke), setCapture(!capture))} />
                    </Col>
                </Row>

            </DetalleLayout>
        ))



    )
}


// export const getStaticPaths: GetStaticPaths = async (ctx) => {
//     const pokemons151 = [...Array(151)].map((value, index) => `${index + 1}`);

//     return {
//         paths: pokemons151.map(id => ({
//             params: { id }
//         })),
//         fallback: false
//     }

// }
// export const getStaticProps: GetStaticProps = async ({ params }) => {
//     const { id } = params as { id: string };
//     const response = await getCacheData.getCachePokemon(parseInt(id));

//     return {
//         props: {
//             pokemon: response
//         }
//     }
// }
export default PokemonDetalle;