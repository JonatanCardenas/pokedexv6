
const db = new PouchDB('pokemons-catched');
//const url = 'https://localhost:7171/api'
const url = "https://apymsa.com.mx/pokedex/api";

function savePokemon(pokemon) {
    // console.log('save pokemon', pokemon);

    pokemon._id = pokemon.id;
    pokemon.state = "NEW";
    // console.log("idpokemon", pokemon);
    return db.post(pokemon).then(() => {
        self.registration.sync.register('new-pokemon');
        const newResp = { ok: true, offline: true };
        return new Response(JSON.stringify(newResp));
    }).catch(error => {
        console.log({ error });
        if (error.name === 'conflict') {

        } else {
            // some other error
        }
    }
    );
}

function updatePokemonLocal(pokemon, state) {
    console.log('update local pokemon', pokemon);

    //var doc = await db.get(pokemon.id)
    db.get(pokemon.id).then(function (doc) {
        console.log("local doc", doc,state);
        return db.put({
            _id: pokemon.id,
            _rev: doc._rev,
            id: pokemon.id,
            nombre: pokemon.nombre,
            pokemonId: pokemon.pokemonId,
            capturado: pokemon.capturado,
            state: state
        });
      }).then(function(response) {
        self.registration.sync.register('update-pokemon');
        const newResp = { ok: true, offline: true };
        return new Response(JSON.stringify(newResp));

    }).catch(function (error) {
        if(error.status === 404){
            console.log("404", error);
            pokemon._id = pokemon.id;
            pokemon.state = state;
            return db.post(pokemon)
            .then(() => {
                self.registration.sync.register('update-pokemon');
                const newResp = { ok: true, offline: true };
                return new Response(JSON.stringify(newResp));
            }).catch(error2 => {
                console.log(error2);
            });
        }
        });

        db.get(pokemon.id).then(function (doc) {
        console.log("localupdatepoke", doc);
        });
}


function postPokemons() {
    const post = [];

    return db.allDocs({ include_docs: true }).then(docs => {
        docs.rows.forEach(row => {

            const doc = row.doc;

            if (doc.state === "NEW") {
                const fetchPom = fetch(url + '/pokemon', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(doc)
                }).then(res => {
                    //update status
                    console.log("local doc", doc);
                    db.put({
                        _id: doc.id,
                        _rev: doc._rev,
                        id: doc.id,
                        nombre: doc.nombre,
                        pokemonId: doc.pokemonId,
                        capturado: doc.capturado,
                        state: "SYNC"
                    });
                }).catch(error =>{
                    //console.log(error);
                });

                post.push(fetchPom);
            }
        });

        return Promise.all(post);
    })
}

function updatePokemons() {
    const post = [];

    return db.allDocs({ include_docs: true }).then(docs => {
        docs.rows.forEach(row => {

            const doc = row.doc;
            if (doc.state === "UPDATE") {
                const fetchPom = fetch(url + '/pokemon/' + doc._id, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(doc)
                }).then(res => {
                    //update status
                    console.log("local doc", doc);
                    db.put({
                      _id: doc._id,
                      _rev: doc._rev,
                      id: doc.id,
                      nombre: doc.nombre,
                      pokemonId: doc.pokemonId,
                      capturado: doc.capturado,
                      state: "SYNC"
                    });
                    
                }).catch(error => {
                    //console.log(error);
                });
                post.push(fetchPom);
            }
        });

        return Promise.all(post);
    })
}
async function getPokemonesLocal() {
    console.log("ALL_POKEMONS");
    return await db.allDocs({ include_docs: true }).then(docs => docs.rows.map(row => row.doc));

}
