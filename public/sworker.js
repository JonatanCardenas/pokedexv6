
import { precacheAndRoute, cleanupOutdatedCaches, matchPrecache } from "workbox-precaching";
import { clientsClaim } from 'workbox-core';
import { registerRoute, NavigationRoute, setCatchHandler } from 'workbox-routing';
import { NetworkOnly, NetworkFirst, StaleWhileRevalidate, CacheFirst, CacheOnly } from 'workbox-strategies';
import { CacheableResponsePlugin } from 'workbox-cacheable-response/CacheableResponsePlugin';
import { ExpirationPlugin } from 'workbox-expiration/ExpirationPlugin';
import { Router } from 'workbox-routing';

//const router = new Router();
//import { PouchDB } from 'pouchdb';

const STATIC_CACHE = 'static-caches';
const DYNAMIC_CACHE = 'dynamic-caches';
const IMAGES_CACHE = 'images-caches';

clientsClaim();
skipWaiting();

const WB_MANIFEST = self.__WB_MANIFEST;
// Precache fallback route and image

WB_MANIFEST.push({
    url: 'https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js',
    revision: null,
});

// WB_MANIFEST.push({
//   url: 'img/fallback.png',
//   revision: null,
// });
// WB_MANIFEST.push({
//     url: '/offline',
//     revision: null,
//   });

precacheAndRoute(WB_MANIFEST);

cleanupOutdatedCaches();

importScripts('https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js')
importScripts('js/sw-db.js');
importScripts('js/sw-utils.js');

/* custom cache rules */


// Imagenes 
registerRoute(
    // Add in any other file extensions or routing criteria as needed.
    ({ url }) => !(url.origin === self.location.origin) && url.pathname.endsWith('.svg'), // Customize this strategy as needed, e.g., by changing to CacheFirst.
    new StaleWhileRevalidate({
        cacheName: IMAGES_CACHE,
        // plugins: [
        //     // Ensure that once this runtime cache reaches a maximum size the
        //     // least-recently used images are removed.
        //     new ExpirationPlugin({ maxEntries: 200 }),
        // ],
    })
);

registerRoute(
    new RegExp('\.(?:png|gif|jpg|jpeg|webp|svg)$'),
    new StaleWhileRevalidate({
        cacheName: IMAGES_CACHE,
        // plugins: [
        //     new CacheableResponsePlugin({
        //         statuses: [0, 200]
        //     }),
        //     new ExpirationPlugin({
        //         maxEntries: 20,
        //         maxAgeSeconds: 12 * 60 * 60
        //     })
        // ]
    }));

registerRoute(
    self.origin,
    new StaleWhileRevalidate({
        cacheName: DYNAMIC_CACHE,
        // plugins: [
        //     new CacheableResponsePlugin({
        //         statuses: [0, 200]
        //     }),
        //     new ExpirationPlugin({
        //         maxEntries: 20,
        //         maxAgeSeconds: 12 * 60 * 60
        //     })
        // ]
    }), 'GET');

//Other resources
registerRoute(

    new RegExp('/_next/static/'),
    new StaleWhileRevalidate({
        cacheName: STATIC_CACHE,
    })
);

// setCatchHandler(({ event }) => {
//     // The FALLBACK_URL entries must be added to the cache ahead of time, either
//     // via runtime or precaching. If they are precached, then call
//     // `matchPrecache(FALLBACK_URL)` (from the `workbox-precaching` package)
//     // to get the response from the correct cache.
//     //
//     // Use event, request, and url to figure out how to respond.
//     // One approach would be to use request.destination, see
//     // https://medium.com/dev-channel/service-worker-caching-strategies-based-on-request-types-57411dd7652c
//     switch (event.request.destination) {
//       case 'document':
//         // If using precached URLs:
//         return matchPrecache('/fallback');
//         // return caches.match('/fallback')
//         break;
//       case 'image':
//         // If using precached URLs:
//         return matchPrecache('img/fallback.png');
//         // return caches.match('/static/images/fallback.png')
//         break;
//       //case 'font':
//       // If using precached URLs:
//       // return matchPrecache(FALLBACK_FONT_URL);
//       // return caches.match('/static/fonts/fallback.otf')
//       // break
//       default:
//         // If we don't have a fallback, just return an error response.
//         return Response.error();
//     }
//   });

//tareas asíncronas
self.addEventListener('sync', e => {
    console.log('SW: Sync');
    console.log('SW e:', e);
    if (e.tag === 'new-pokemon') {
        // postear a BD cuando hay conexión
        const respuesta = postPokemons();
        e.waitUntil(respuesta);
    }

    if (e.tag === 'update-pokemon') {
        // actualizar a BD cuando hay conexión
        const respuesta = updatePokemons();
        e.waitUntil(respuesta);
    }

});

//peticiones fetch
self.addEventListener('fetch', async function (event) {
    //console.log("PETICION_FETCH ", event.request.clone().url);


    if (event.request.url.includes('api/Notifications')) {
        console.log("peticion notifications")
        console.log(event.request.clone())
        event.respondWith(fetch(event.request.clone()))
    }
    else if (event.request.url.includes('api/Pokemon') && event.request.clone().method !== 'GET') {
        //console.log("manageApiPokemons")
        await manageApiPokemons(DYNAMIC_CACHE, event.request);

    } else {
        event.respondWith(
            //Se hace peticion a internet 
            fetch(event.request.clone())
                .then(response => {
                    //console.log("response ", response);

                    //Si el response contiene un ok:false retorna cache 
                    if (!response.ok) {
                        return caches.match(event.request.clone().url)
                    }

                    //Si obtiene respuesta de internet la guarda en el cache con la url como llave para acceder a ella 
                    caches.open(DYNAMIC_CACHE)
                        .then(cache => {
                            //console.log("put cache: " + event.request.clone().url)
                            cache.put(event.request.clone().url, response)
                        })
                    // devuelve la respuesta de internet 
                    return response.clone();
                })
                .catch(error => {
                    //Si cae en el catch es porque no se pudo hacer la peticion a internet 
                    //Aqui busca en cache por medio de la llave que es la url 
                    if (event.request.url.includes('api/Pokemon')
                        && event.request.clone().method === 'GET') {
                        //console.log()
                        return pokemonsCatchs().then((pokemon) => {
                            return new Response(JSON.stringify(pokemon)
                                , {
                                    headers: { 'Content-Type': 'application/json' }
                                }
                            )
                        });

                    }

                    return caches.match(event.request.clone().url)
                })
        );
    }
});

self.addEventListener('push', e => {
    console.log(e.data.text())
    var data = JSON.parse(e.data.text())

    const title = data.Titulo;
    const options = {
        body: data.Body,
        icon: '../poke.ico',
        badge: '../poke.ico'
    }
    e.waitUntil(self.registration.showNotification(title, options))
});


