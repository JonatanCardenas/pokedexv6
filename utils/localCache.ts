import { PokemonList } from "@/interfaces";

const getCachePokemon = async (id: number) => {
    const res = await fetch('http://localhost:3000/_next/data/development/index.json')
        .then(data => {
            return data.json();
        })

    const pokemones: PokemonList[] = res.pageProps.pokemons;
    return pokemones.find(x => x.id === id)!;
}
const deleteCache = () => {
    caches.keys().then((names) => {
        names.forEach((name) => {
            caches.delete(name);
        });
    });
    alert('Complete Cache Cleared')

}
export default {
    deleteCache,
    getCachePokemon
}