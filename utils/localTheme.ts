import { darkTheme } from "@/themes";
import { ThemeConfig } from "antd";
import { pokemonStatEnum } from "./pokemonStatEnum";

type localThemeCurrente = {
    darkTheme: boolean
}
const getLocalTheme = (): localThemeCurrente => {
    const themeStorage = typeof window !== 'undefined' ? localStorage.getItem('defaultTheme') : null;
    const objeto = { 'darkTheme': false }
    if(typeof window !== 'undefined'){
        console.log('themeStorage', localStorage.getItem('defaultTheme'));
        const themeStorage = localStorage.getItem('defaultTheme');
        if (themeStorage == null) return objeto;
        return JSON.parse(themeStorage!)
    }
    return objeto;
}
const setLocalTheme = (darkTheme: boolean) => {
    localStorage.removeItem("defaultTheme");
    localStorage.setItem('defaultTheme', JSON.stringify(darkTheme === true ? { 'darkTheme': true } : { 'darkTheme': false }));
}
const onColor = (stat: string) => {
    return pokemonStatEnum[stat as keyof typeof pokemonStatEnum]
}
export default {
    getLocalTheme,
    setLocalTheme,
    onColor
}