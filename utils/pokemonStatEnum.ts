export enum pokemonStatEnum {
    HP = '#d53a43',
    ATK = '#fda827',
    DEF = '#0096f5',
    SA = '#8eb0c4',
    SPD = '#a5a5a5',
    EXP = '#327C37',
}