export enum pokemonTypesEnum {
    grass= '#00ca91',
    fire= '#e95c4d',
    bug ='#aedf78',
    water ='#43ccff',
    normal='#a5a5a5',
    poison ='#611380',
    electric ='#f9be00',
    ground ='#bfac21',
    fairy ='#f87ea7',
    fighting ='#e81319',
    psychic= '#8a0532',
    ghost= '#8e55a4',
    rock= '#776a3e',
    ice= '#66d1e5',
    flying= '#5eb9b2',
    dark= '#2d221c',
    dragon= '#29036a',
    steel= '#7b8e8a'
    //unknown= '#454545',
    //shadow= '#4f4f4f'
    
}

