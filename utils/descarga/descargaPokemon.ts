import PouchDB from 'pouchdb';
import { Pokemon, PokemonCatch, PokemonList } from "@/interfaces";
const url = "https://apymsa.com.mx/pokedex/api";
const db = new PouchDB('pokemons-catched')
export async function getPokemones () { 
    try {
        const result = await db.allDocs({ include_docs: true });
        const docs = result.rows.map(row => row.doc);

        if (result.total_rows > 0){
            await Promise.all(
                docs.map(async (doc) => {
                  if (doc) {
                    await db.remove(doc);
                  }
                })
              );
          
              console.log('Filas eliminadas con éxito');
        }
        // Eliminar cada documento
        
      } catch (error) {
        console.error('Error al eliminar las filas', error);
      }


      let data: PokemonCatch[] = [];
      const peticion = await fetch(url + '/Pokemon');
      data = await peticion.json();
     data.forEach(c=>{
      return db.put({
        _id: c.id,
        id:c.id,
        pokemonId:c.pokemonId,
        capturado: c.capturado,
        state: 'SYNC'
      }).catch(error =>{
        console.log({ error });
              if (error.name === 'conflict') {
      
              } else {
                  // some other error
              }
      })
     })
      /* data.forEach(c =>{
          return db.post(c).then(() => {
             
          }).catch(error => {
              console.log({ error });
              if (error.name === 'conflict') {
      
              } else {
                  // some other error
              }
          }
          );
      })
       */
      return data;
      console.log(data) 
  }