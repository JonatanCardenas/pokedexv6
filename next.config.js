/** @type {import('next').NextConfig} */

const withPWA = require("next-pwa")({
  dest: "public",
  register: true,
  skipWaiting: true,
  swSrc: 'public/sworker.js',
});
 
const nextConfig = withPWA({
  reactStrictMode: true,
  images: {
    domains: ['raw.githubusercontent.com'] ,
    unoptimized: true 
   }
});
 
module.exports = nextConfig
