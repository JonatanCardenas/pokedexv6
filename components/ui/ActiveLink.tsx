import { FC } from "react";
import Link from "next/link";
import styles from './Navbar.module.css';
import {theme} from 'antd';
import localTheme from '@/utils/localTheme';
interface Props{

    href : string;
    text: string;

}
export const ActiveLink : FC<Props> = ( {text, href} ) => {
    const {useToken} = theme;
    const {token} = useToken()
    const {darkTheme} = localTheme.getLocalTheme();
    return (
        
        <Link href={ href } legacyBehavior>
            <a  className={styles['nav__link']} style={{color: darkTheme === false ? '#B66206':token.colorText}}>{ text }</a>
        </Link>
    );
};