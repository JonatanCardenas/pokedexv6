
import { pokemonTypesEnum } from "@/utils";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button, Col, Image, Row, Typography, theme } from "antd";
import Link from "next/link";
import globalStyle from '../../styles/Global.module.css';
import PokemonStyle from '../../pages/Pokemon/Pokemon.module.css';
import { FC } from "react";

const { useToken } = theme;
type pokemon = {
    types: string[];
    id: number;
    img: string;
    name: string;
}
export const DetallePrincipalPokemon: FC<pokemon> = ({ types, id, img, name }) => {
    const { token } = useToken();

    const tipopokemon: string = types[0];
    const colorPokemon: string = pokemonTypesEnum[tipopokemon as keyof typeof pokemonTypesEnum]
    return (
        <>
            <Row gutter={[24, 24]} style={{ marginRight: '0px', background: colorPokemon }} >
                <Col span={12}>
                    <Link href='/' passHref >
                        <Button type="text" shape='circle'
                            icon={<ArrowLeftOutlined
                                className={globalStyle['icon-back']}
                                style={{
                                    color: token.colorIcon,
                                    transform: 'scale(1)',
                                    padding: '5px'
                                }} />} />
                        <Typography.Text
                            strong
                            style={{ paddingLeft: '10px', color: token.colorIcon }}

                            className={PokemonStyle['text-sizes-weight']}
                        >
                            Pokedex
                        </Typography.Text >
                    </Link>
                </Col>
                <Col span={12} className='gutter-row' >
                    <div className={PokemonStyle['pokemon-serie']}>
                        <Typography.Text strong className={PokemonStyle['text-sizes-weight']} style={{ color: token.colorIcon }}>#{id < 10 ? `00${id}` : id < 100 ? `0${id}` : id}</Typography.Text >
                    </div>
                </Col>
            </Row>
            <Row >
                <Col
                    style={{ background: colorPokemon }}
                    className={PokemonStyle['contenedor-imagen']}
                >
                    <Image
                        style={{ maxHeight: '200px' }}
                        src={img}
                        alt={name}
                    />
                </Col>
            </Row>
        </>
    )
}
