import { Col, Row, Typography } from 'antd'
import React, { FC } from 'react'
import PokemonStyle from '../../pages/Pokemon/Pokemon.module.css';
type Props = {
    pokemonWeight: number;
    pokemonHeight: number;
}
export const MedidasPokemon: FC<Props> = ({ pokemonHeight, pokemonWeight }) => {
    return (
        <>
            <Col span={12}>
                <Row >
                    <Col span={24} className={PokemonStyle['centrar-textos']}>
                        <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }}  >{pokemonWeight} KG</Typography.Title>
                        <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Weight</Typography.Title>
                    </Col >
                </Row>
            </Col>
            <Col span={12}>
                <Row >
                    <Col span={24} className={PokemonStyle['centrar-textos']}>
                        <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }} >{pokemonHeight}  M</Typography.Title>
                        <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Height</Typography.Title>
                    </Col >
                </Row>
            </Col>
        </>
    )
}
