import { pokemonTypesEnum } from '@/utils'
import { Col, Tag } from 'antd'
import React, { FC } from 'react'

type Props = {
    types: string[]
}
export const PokeTipos: FC<Props> = ({ types }) => {
    return (
        <>
            {
                types.map((type) => (
                    <Col key={type} >
                        <Tag color={pokemonTypesEnum[type as keyof typeof pokemonTypesEnum]} bordered style={{ width: '100px', textAlign: 'center', borderRadius: '10px' }}>
                            {type}
                        </Tag>
                    </Col>
                ))
            }
        </>
    )
}
