import { PokemonList } from '@/interfaces'
import { Row, Typography } from 'antd'
import React, { FC } from 'react'
import { PokeTipos } from './PokeTipos'
import { MedidasPokemon } from './MedidasPokemon'
import { EstadisticaPokemon } from './EstadisticaPokemon';
import PokemonStyle from '../../pages/Pokemon/Pokemon.module.css';
const { Title } = Typography;

type Props = {
    pokemon: PokemonList
}
export const ContenidoPokemon: FC<Props> = ({ pokemon }) => {
    return (
        <div className={PokemonStyle['contenedor-principal']}>
            <div className={PokemonStyle['contenedor-derecho']}>
            </div>
            <div className={PokemonStyle['contenedor-centro']}>
                <Row justify={'center'}>
                    <Title >
                        {pokemon.name}
                    </Title>
                </Row>
                <Row justify={'center'}>
                    <PokeTipos types={pokemon.types} />
                </Row>
                <Row>
                    <MedidasPokemon pokemonHeight={pokemon.height} pokemonWeight={pokemon.weight} />
                </Row>
                <Row >
                    <EstadisticaPokemon pokemon={pokemon.stats} />
                </Row>
            </div>
            <div className={PokemonStyle['contenedor-izquierdo']}>

            </div>

        </div>
    )
}
