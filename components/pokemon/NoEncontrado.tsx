import { Button, Typography, theme } from 'antd';
import React from 'react'
import pokebola from '../../public/img/pokeball.gif';
import Image from 'next/image';
import { ArrowLeftOutlined } from '@ant-design/icons';
import globalStyle from '../../styles/Global.module.css';
import Link from "next/link";

export const NoEncontrado = () => {
    const { useToken } = theme;
    const { token } = useToken()
    return (
        <div style={{ height: '100vh', background: token.colorBgContainer }}>

            <div style={{ display: 'flex', flexDirection: 'column', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <Image src={pokebola} alt='logo pokemón' style={{ width: '200px', height: '200px' }} />

                <Typography.Title
                    style={{ paddingLeft: '10px' }}
                >
                    POKEMON NO ENCONTRADO
                </Typography.Title >
                <Link href='/' passHref >
                    <Button type="text" shape='circle'
                        icon={<ArrowLeftOutlined
                            className={globalStyle['icon-back']}
                            style={{
                                color: token.colorIcon,
                                transform: 'scale(1)',
                                padding: '5px'
                            }} />} />

                </Link>

            </div>
        </div>
    )
}
