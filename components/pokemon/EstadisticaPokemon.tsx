import { Col, Progress, Row, Typography } from 'antd'
import React, { FC } from 'react'
import localPokemon from "@/utils/localPokemon";
import localTheme from "@/utils/localTheme";
import PokemonStyle from '../../pages/Pokemon/Pokemon.module.css';
import { PokemonStats } from '@/interfaces';
type Props = {
    pokemon: PokemonStats[]
}

export const EstadisticaPokemon: FC<Props> = ({ pokemon }) => {
    return (
        <>
            <Col span={24} className={PokemonStyle['centrar-textos']}>
                <Typography.Title >
                    Base Stats
                </Typography.Title>
            </Col>
            {
                pokemon.filter(x => x.stat != 'speed').map((stat) => (
                    <Col span={24} key={stat.stat}>
                        <Row >
                            <Col span={3} className={PokemonStyle['centrar-textos']}>
                                <Typography.Text style={{ fontWeight: 'bold' }} >{localPokemon.onStateName(stat.stat)}</Typography.Text>
                            </Col>
                            <Col span={20}>
                                <Progress type='line' percent={stat.base} strokeColor={localTheme.onColor(localPokemon.onStateName(stat.stat))} showInfo={false} />
                            </Col>
                        </Row>
                    </Col>

                ))}
        </>
    )
}
